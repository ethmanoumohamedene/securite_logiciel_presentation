<?php
session_start();

if (!isset($_SESSION["username"])) {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bienvenue</title>
</head>
<body>
    <h2>Bienvenue, <?php echo $_SESSION["username"]; ?> !</h2>
    <p>C'est votre page de bienvenue.</p>
    <a href="logout.php">Se déconnecter</a>
</body>
</html>
