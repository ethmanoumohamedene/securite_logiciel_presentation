
<?php
include_once 'common.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $error_message = loginUser_sans_protection($username , $password); //sans aucun mesure contre les deux attaques 
    //$error_message = loginUser($username, $password); //avec les mesures necessaires contre brute force et injection sql
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'Identification</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            background-color: #3498db; /* Couleur de fond */
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        h2 {
            text-align: center;
        }

        form {
            background-color: rgba(255, 255, 255, 0.8);
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            width: 300px;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #2ecc71; /* Couleur du bouton */
            color: #fff;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #27ae60; /* Couleur du bouton au survol */
        }

        p {
            color: red;
            text-align: center;
            margin-top: 10px;
        }

        /* Ajoutez ce style pour l'avatar */
        .avatar {
            display: block;
            margin: 0 auto;
            width: 100px; /* Ajustez la taille de l'avatar selon vos besoins */
            border-radius: 50%;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <form method="post">
        <img class="avatar" src="avatar.jpg" alt="Avatar"> <!-- Remplacez par le chemin de votre avatar -->
        <h2>Page d'Identification</h2>
        <?php if (isset($error_message)) : ?>
            <p><?php echo $error_message; ?></p>
        <?php endif; ?>
        <label for="username">Nom d'utilisateur :</label>
        <input type="text" name="username" required>
        
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" required>

        <input type="submit" value="Se connecter">
    </form>
</body>
</html>
