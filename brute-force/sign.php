<?php
include_once 'common.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["reg_username"];
    $password = password_hash($_POST["reg_password"], PASSWORD_BCRYPT);
    $registration_error = registerUser_sans_protection($username , $password); // sans aucun mesure contre injection sql
    //$registration_error = registerUser($username, $password); // avec les mesures necesaire contre injection sql
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .container {
            background-color: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        form {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        label {
            margin-bottom: 8px;
        }

        input {
            margin-bottom: 16px;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 200px;
        }

        .submit-btn {
            background-color: #4caf50;
            color: white;
            cursor: pointer;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
        }

        .error-message {
            color: red;
            margin-bottom: 16px;
        }

        .success-message {
            color: green;
            margin-bottom: 16px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Page d'Inscription</h2>

        <?php if (isset($registration_error)) : ?>
            <p class="error-message"><?php echo $registration_error; ?></p>
        <?php endif; ?>

        <form method="post">
            <label for="reg_username">Nom d'utilisateur :</label>
            <input type="text" name="reg_username" required><br>

            <label for="reg_password">Mot de passe :</label>
            <input type="password" name="reg_password" required><br>

            <input type="submit" class="submit-btn" value="S'inscrire">
        </form>
    </div>
</body>
</html>
