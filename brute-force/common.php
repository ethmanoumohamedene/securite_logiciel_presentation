<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

function connectDB() {
    $db_host = "localhost";
    $db_user = "kali";
    $db_password = "kali";
    $db_name = "monsite";

    $conn = new mysqli($db_host, $db_user, $db_password, $db_name);

    if ($conn->connect_error) {
        die("Échec de la connexion à la base de données: " . $conn->connect_error);
    }

    return $conn;
}

function closeDB($conn) {
    $conn->close();
}

function loginUser_sans_protection($username, $password) {
    $conn = connectDB();

    $query = "SELECT id, password_hash FROM users WHERE username = '$username'";
    $result = $conn->query($query);

    if ($result && $result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $user_id = $row['id'];
        $password_hash = $row['password_hash'];

        if (password_verify($password, $password_hash)) {
            $_SESSION["username"] = $username;

            // Mettre à jour la base de données avec le nouveau nombre de tentatives
            $update_query = "UPDATE users SET login_attempts = 0 WHERE id = $user_id";
            $conn->query($update_query);

            $conn->close();
            header("Location: welcome.php");
            exit();
        } else {
            $error_message = "Identifiants invalides.";
        }
    } else {
        $error_message = "Identifiants invalides.";
    }

    $conn->close();
    return $error_message;
}



function loginUser($username, $password) {
    $conn = connectDB();

    $stmt = $conn->prepare("SELECT id, password_hash, login_attempts, blocked FROM users WHERE username = ?");
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        $stmt->bind_result($user_id, $password_hash, $login_attempts, $blocked);
        $stmt->fetch();

        if ($blocked) {
            $error_message = "Votre compte est bloqué. Veuillez contacter l'administrateur.";
        } elseif (password_verify($password, $password_hash)) {
            $_SESSION["username"] = $username;

            // Initialiser le nombre de tentatives après une connexion réussie
            $login_attempts = 0;

            // Mettre à jour la base de données avec le nouveau nombre de tentatives
            $stmt_update = $conn->prepare("UPDATE users SET login_attempts = ? WHERE id = ?");
            $stmt_update->bind_param("ii", $login_attempts, $user_id);
            $stmt_update->execute();
            $stmt_update->close();

            header("Location: welcome.php");
            exit();
        } else {
            $login_attempts++;

            if ($login_attempts >= 3) {
                $blocked = 1; // Bloquer le compte après 3 tentatives
                $stmt_blocked = $conn->prepare("UPDATE users SET login_attempts = ?, blocked = ? WHERE id = ?");
                $stmt_blocked->bind_param("iii", $login_attempts, $blocked, $user_id);
                $stmt_blocked->execute();
                $stmt_blocked->close();

                header("Location: blocked.php");
                exit();
            }

            $stmt_attempt = $conn->prepare("UPDATE users SET login_attempts = ? WHERE id = ?");
            $stmt_attempt->bind_param("ii", $login_attempts, $user_id);
            $stmt_attempt->execute();
            $stmt_attempt->close();

            $error_message = "Identifiants invalides.";
        }
    } else {
        $error_message = "Identifiants invalides.";
    }

    $stmt->close();
    closeDB($conn);

    return $error_message;
}

function registerUser_sans_protection($username, $password, $login_attempts = 0, $blocked = 0) {
    $conn = connectDB();

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    $query = "INSERT INTO users (username, password_hash, login_attempts, blocked) VALUES ('$username', '$hashed_password', $login_attempts, $blocked)";

    if ($conn->query($query) === TRUE) {
        header("Location: welcome.php");
        exit();
    } else {
        $registration_error = "Erreur lors de l'inscription. Veuillez réessayer.";
    }

    closeDB($conn);

    return $registration_error;
}



function registerUser($username, $password,  $login_attempts = 0, $blocked = 0) {
    $conn = connectDB();

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    $stmt = $conn->prepare("INSERT INTO users (username, password_hash, login_attempts, blocked) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssii", $username, $hashed_password, $login_attempts, $blocked);

    if ($stmt->execute()) {
        header("Location: welcome.php");
        exit();
    } else {
        $registration_error = "Erreur lors de l'inscription. Veuillez réessayer.";
    }

    $stmt->close();
    closeDB($conn);

    return $registration_error;
}
?>

